<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LeaderBoard;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;
class AuthController extends Controller
{
    public function register(Request $request){

    $validated = $request->validate([
        'username' => 'required|unique:users|max:255',
        'email' => 'required|unique:users|max:255',
        'password' => 'required',
        // 'device_id'=>'required',
    ]);

    $user = User::create([
        'username' => $request->username,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'device_id'=>$request->device_id,
        'lives'=>5,
    ]);

    $user->addRole('user');

    LeaderBoard::create([
        'user_id'=>$user->id,
        'score'=>0,
        'rank'=>0,
        'date_time'=>null,
    ]);
    return response()->json([
        'status'=>200,
        'message'=>'account created successfully',
        'user'=>$user,
    ]);
    }
    public function login(Request $request){

        $validated = $request->validate([
            'email' => 'required',
            'password' => 'required',
            'device_id'=>'required',
        ]);

        $user = User::where('email',$request->email)->first();
        // $user->device_id = $request->device_id;
        $user->save();
        $ranking = LeaderBoard::where('user_id',$user->id)->first();
        if($user){
            if(Hash::check($request->password, $user->password))
            {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                return response()->json([
                    'status'=>200,
                    'message'=>'user authenticate successfully',
                    'user'=>$user,
                    'ranking'=>$ranking,
                    'token'=>$token,
                ]);
            }
            else{
                return response()->json([
                    'status'=>200,
                    'message'=>'invalid credentials',
                ]);
            }

        }
        else{
            return response()->json([
                'status'=>404,
                'message'=>'invalid email address',
            ],404);
        }


    }

    public function profile_update(Request $request){
        $user = auth()->user();
        if(isset($request->country)){
            $user->country = $request->country;
        }
        if($request->hasFile('profile')){
            $image = $request->file('profile');
            $imageName = time() .uniqid(). '.' . $image->getClientOriginalExtension();
            $image->storeAs('images', $imageName, 'public'); // 'public' is the disk name; you can change it as needed.
            $image_path = 'storage/images/' . $imageName;
            $user->profile_url = $image_path;
        }
        $user->save();

        return response()->json([
            'status'=>200,
            'message'=>'profile has been updated successfully'
        ]);
    }
}
