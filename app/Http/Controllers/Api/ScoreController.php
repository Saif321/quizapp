<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LeaderBoard;
use Illuminate\Http\Request;
use App\Models\User;
class ScoreController extends Controller
{
    public function update_score(Request $request){
        $validated = $request->validate([
            'score' => 'required|numeric',
        ]);

        $user = auth()->user();
        $user_leaderboard = LeaderBoard::where('user_id',$user->id)->latest()->first();
        $user_leaderboard->update([
            'score'=>$user_leaderboard->score + $request->score,
            'date_time'=>date('Y-m-d H:i:s'),
            'user_id'=>$user->id,
        ]);

        $leaderboard =LeaderBoard::all()->sortByDesc('score');
        $rank = $leaderboard->search(function ($item) use ($user_leaderboard) {
            return $item->id === $user_leaderboard->id;
        });
        $user_leaderboard->update([
            'rank'=>$rank+1,
        ]);
        return response()->json([
            'status' => 'success',
            'message'=>'leaderboard updated successfully',
            'leaderboard'=>LeaderBoard::where('user_id',$user->id)->latest()->first(),
        ]);


    }
}
