<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\GameRecord;
use App\Models\Question;
use App\Models\WaitingRoom;
use Illuminate\Http\Request;
use App\Jobs\Matchmaking;
use App\Models\MatchRoom;
use App\Models\User;
class GameController extends Controller
{
    public function play_game(Request $request){
        auth()->user()->update([
            'matching_since'=>date('Y-m-d H:i:s'),
            'game_status'=>'matching',
        ]);
        $users = User::where('game_status','matching')->where('matching_since','>=',date('Y-m-d H:i:s',strtotime('-30 seconds')))->inRandomOrder()->first();
        $room = 0;
        $space = WaitingRoom::where('user_id',auth()->user()->id)->latest()->first();
        if($space){
            $space->delete();
        }
        $waitingroom = WaitingRoom::create([
            'user_id'=>auth()->user()->id,
        ]);
        if($waitingroom){
                 // Fetch the top 2 users from the WaitingRoom
        $users = WaitingRoom::orderBy('created_at')->take(2)->get();

        // Make sure there are at least 2 users in the WaitingRoom
        if ($users->count() >= 2) {
            // Create a new MatchRoom record
          $room =  MatchRoom::create([
                'player1_id' => $users[0]->user_id,
                'player2_id' => $users[1]->user_id,
          ]);

            // Delete the users from the WaitingRoom
            WaitingRoom::whereIn('user_id', [$users[0]->user_id, $users[1]->user_id])->delete();
        }
        if($room){
            // Matchmaking::dispatch();
            return response()->json([
                'status'=>200,
                'game_room'=>$room,
            ]);
        }
        else{
            // sleep(30);
            return response()->json([
                'status'=>200,
                'message'=>'waiting for a player'
            ]);
        }

        }
    }

    public function update_game_score(Request $request){
         $validated = $request->validate([
            'room_id' => 'required|numeric',
            'question_id' => 'required|numeric',
            'player1_answer' => 'required|numeric',
            'player2_answer' => 'required|numeric',
            'game_status' => 'required|numeric',
        ]);

        $question = Question::find($validated['question_id']);
        if(!$question){
            return "invalid question id";
        }
        $room = MatchRoom::find($validated['room_id']);
        if($room->status == "finished"){
            return response()->json([
                'status'=>404,
                'message'=>"no game room found"
            ],404);
        }
        else{
            $record = GameRecord::create([
                'room_id'=>$validated['room_id'],
                'player1_answer'=>$validated['player1_answer'],
                'player2_answer'=>$validated['player2_answer'],
                'correct_answer'=>$question->answer,
            ]);
            if($request->game_status){
                return response()->json([
                    'status'=>200,
                    'message'=>"record saved",
                    'record'=>$record,
                ],200);
            }
            else{
                $room->update([
                    'status'=>'finished',
                ]);
               $gameRecords = GameRecord::where('room_id', $validated['room_id'])->get();
               $player1_correct_points = $gameRecords->sum(function ($record) {
                return $record->player1_answer === $record->correct_answer ? 1 : 0;
                });

                $player1_wrong_points = $gameRecords->count() - $player1_correct_points;

                $player2_correct_points = $gameRecords->sum(function ($record) {
                    return $record->player2_answer === $record->correct_answer ? 1 : 0;
                });

                $player2_wrong_points = $gameRecords->count() - $player2_correct_points;

                if ($player1_correct_points > $player2_correct_points) {
                    $winner = $room->player1_id;
                } elseif ($player1_correct_points < $player2_correct_points) {
                    $winner = $room->player2_id;
                } else {
                    $winner = 'tie';
                }
                $winner_user = User::find($winner);
                $winner_user->update([
                    'points'=>$winner_user->score + 10,
                ]);
                $result = [
                    'player1_correct_points' => $player1_correct_points,
                    'player1_wrong_points' => $player1_wrong_points,
                    'player2_correct_points' => $player2_correct_points,
                    'player2_wrong_points' => $player2_wrong_points,
                    'winner_score'=>10,
                    'winner' => $winner == "tie" ? "tie" : $winner_user,
                ];

                return response()->json([
                    'status'=>200,
                    'result'=>$result,
                ]);
            }
        }

    }
}
