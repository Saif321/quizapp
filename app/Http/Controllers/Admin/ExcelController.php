<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\QuestionsImport;

class ExcelController extends Controller
{
    public function uploadExcel(Request $request)
    {
        // Validate the uploaded file
        $request->validate([
            'excel_file' => 'required|mimes:xlsx,xls',
        ]);

        // Process the Excel file and import data into the "questions" table
        Excel::import(new QuestionsImport, $request->file('excel_file'));

        // Redirect back with a success message
            return "success";

    }
}
