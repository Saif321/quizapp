<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class QuestionController extends Controller
{
    public function questions_index(){
        return view('admin.questions.index');
    }

    public function get_all_questions(Request $request){
        $questions = Question::all();

        if (request()->ajax()) {
            return DataTables::of($questions)
                ->addIndexColumn()
                ->editColumn('status', function ($question) {
                    $current_status = $question->status;
                    return ($question->status == 1) ? '<a type="button"  <span onclick="change_status('  . $question->id . ', ' . $current_status . ')" class="badge badge-success">active</span></a>' : '<a type="button"  <span onclick="change_status(' . $question->id . ')" class="badge badge-danger">disabled</span></a>';
                })
                ->editColumn('answer_status', function ($question) {
                    return ($question->answer == 1) ? 'TRUE' : 'FALSE';
                })
                ->editColumn('action', function ($question) {
                         return  '<a type="button" onclick="edit_question_modal(' . $question->id . ')" class="btn btn-success"><i class="fa-solid fa-pencil"></i></a>
                           <a type="button" onclick="delete_question('.$question->id.')" class="btn btn-primary"><i class="fa-solid fa-trash"></i></a>';
                       })
                ->rawColumns(['status','action'])
                ->toJson();
        }
        return view('admin.questions.index');
    }

    public function admin_question_add(Request $request){
        $request['question'] = $request->question_input;
        $request['answer'] = $request->answer_input;
        $validated = $request->validate([
            'question' => 'required|unique:questions',
            // 'option1' => 'required',
            // 'option2' => 'required',
            'answer' => 'required',
        ]);

        if($request->answer === 'TRUE' || $request->answer === "TRUE" || $request->answer === TRUE || $request->answer === 'true' ){
            $request->answer = 1;
        }
        else{
            $request->answer = 0;
        }
        $question = Question::create([
            'question' => $request->question_input,
            'answer' => $request->answer,
            // 'option1' => $request->option1,
            // 'option2' => $request->option2,
            'status' => 1,
        ]);

        if($question){
         return "success";
        }
    }


    public function admin_question_delete($id){
        $question = Question::find($id)->delete();
        if($question){
            return "success";
        }

    }

    public function admin_get_question($id){
        $question = Question::find($id);
        return $question;
    }

    public function admin_update_question(Request $request,$id){

        $question = Question::find($id)->update([
            'question'=>$request->question,
            // 'option1'=>$request->option1,
            // 'option2'=>$request->option2,
            'answer'=>$request->answer,
        ]);
        return "success";
    }

    public function admin_question_status($id){
        $question = Question::find($id);
        if($question->status){
            $question->status = 0;
            $question->save();
        }
        else{
            $question->status = 1;
            $question->save();
        }
        return "success";
    }
}
