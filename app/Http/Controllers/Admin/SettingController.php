<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SettingController extends Controller
{
    public function settings_index(){
        $settings = Setting::latest()->first();
        return view('admin.settings.index', compact('settings'));
    }

    public function update_settings(Request $request){
        $validated = $request->validate([
            'default_lives' => 'required|numeric',
            'default_waiting_time_for_matching' => 'required|numeric'
        ]);
        $settings = Setting::latest()->first();
        $set = $settings->update([
            'default_lives'=>$request->default_lives,
            'default_waiting_time_for_matching'=>$request->default_waiting_time_for_matching
        ]);
        if($set){
            Alert::success('Success', 'Settings updated successfully');
            return response()->redirectToRoute('settings');
        }
    }
}
