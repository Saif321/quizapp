<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;
class UserController extends Controller
{
    public function get_all_users_index(Request $request){
        return view('admin.users.index');
    }

    public function get_all_users(Request $request){
        $users = User::whereHasRole('user')->get();

        if (request()->ajax()) {
            return DataTables::of($users)
                ->addIndexColumn()
                ->editColumn('status', function ($user) {
                    $currentUser = $user->status;
                    return ($user->status == 1) ? '<a type="button"  <span onclick="change_status(' . $user->id .','.$currentUser. ')" class="badge badge-success">active</span></a>' : '<a type="button"  <span onclick="change_status(' . $user->id . ')" class="badge badge-danger">disabled</span></a>';
                })
                // ->editColumn('action', function ($user) {
                //          return  '<a type="button" onclick="remove_user(' . $user->id . ')" class="btn btn-success"><i class="fa-solid fa-flag"></i></a>
                //            <a type="button" onclick="banned_user(' . $user->id .','.$user->id.')" class="btn btn-primary"><i class="fa-solid fa-user-large-slash"></i></a>';
                //        })
                ->rawColumns(['status'])
                ->toJson();
        }
        return view('admin.users.index');
    }

    public function admin_change_status($id){
        $user = User::find($id);
        if($user){
            if($user->status){
                $user->status = 0;
                $user->save();
                return "success";
            }
            else{
                $user->status = 1;
                $user->save();
                return "success";
            }
        }

        return "user not found";
    }

}
