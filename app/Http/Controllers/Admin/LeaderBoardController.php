<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LeaderBoard;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
class LeaderBoardController extends Controller
{
    public function leaderboard(Request $request){
        return view('admin.leaderboard.index');
    }

    public function get_leaderboard(){
        $leaderboards = LeaderBoard::with('user')->get();
        if (request()->ajax()) {
            return DataTables::of($leaderboards)
                ->addIndexColumn()
                ->editColumn('username', function ($leaderboard) {

                    return $leaderboard->user->username;
                })
                ->editColumn('email', function ($leaderboard) {

                    return $leaderboard->user->email;
                })
                ->editColumn('datetime', function ($leaderboard) {

                    // Assuming you have a DateTime object, you can convert it to a human-readable format like this:
                    $dateTime = Carbon::parse($leaderboard->date_time); // Replace $yourDateTime with your actual DateTime object

                    // Format it as a readable date and time
                    return $dateTime->format('F j, Y h:i A'); // Example format

                    // You can choose any format that suits your needs. The format() method takes a format string as its argument.

                })
                // ->editColumn('action', function ($leaderboard) {
                //          return  '<a type="button" onclick="edit_leaderboard_modal(' . $leaderboard->id . ')" class="btn btn-success"><i class="fa-solid fa-pencil"></i></a>
                //            <a type="button" onclick="delete_leaderboard('.$leaderboard->id.')" class="btn btn-primary"><i class="fa-solid fa-trash"></i></a>';
                //        })
                ->rawColumns([])
                ->toJson();
        }
        return view('admin.leaderboards.index');
    }
}
