<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchRoom extends Model
{
    protected $guarded = [];
    use HasFactory;


    public function gamerecord(){
        return $this->hasMany(GameRecord::class, 'room_id', 'id');
    }
}
