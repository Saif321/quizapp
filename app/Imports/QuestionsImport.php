<?php

namespace App\Imports;

use App\Models\Question; // Make sure to import your Question model
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class QuestionsImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // dd($row);
        // Map Excel columns to your "questions" table columns
        $question = Question::where('question',$row['questions'])->first();
        if($question)
            return $question;
        // if($row['option_1'] != $row['option_2']){
            // if($row['anwser'] === $row['option_1'] || $row['anwser'] === $row['option_2']){
                return new Question([
                    'question' => $row['questions'],
                    'answer' => $row['anwser'],
                    'status'=>$row['status'] === 'Active' ? 1 : 0,
                    // Map other columns accordingly
                ]);
            // }
    //     }



    }
}

