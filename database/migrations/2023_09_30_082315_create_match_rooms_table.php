<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('match_rooms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('player1_id');
            $table->unsignedBigInteger('player2_id');
            $table->BigInteger('player1_score')->nullable();
            $table->BigInteger('player2_score')->nullable();
            $table->time('game_time')->nullable();
            $table->enum('status',['in_game','finished'])->default('in_game');
            $table->enum('result',['lost','win','tie'])->nullable();
            // Define foreign key constraints
            $table->foreign('player1_id')->references('id')->on('users');
            $table->foreign('player2_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('match_rooms');
    }
};
