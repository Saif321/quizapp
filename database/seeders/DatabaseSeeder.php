<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Role;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use App\Models\User;
use Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $user = User::create([
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password'=>Hash::make('password'),
        ]);

        $userrole = Role::create([
            'name' => 'user',
            'display_name' => 'Project User', // optional
            'description' => 'User is the normal user of a given project', // optional
        ]);

        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'Project Admin', // optional
            'description' => 'User is the admin of a given project', // optional
        ]);

        $user->addRole($admin);

        Setting::create([
            'default_lives'=>5,
            'default_waiting_time_for_matching'=>30,
        ]);
    }
}
