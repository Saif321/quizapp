@extends('admin.layouts.sidebar')

@push('start-style')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/table/datatable/dt-global_style.css') }}">
    <link href="{{ asset('assets/assets/css/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/file-upload/file-upload-with-preview.min.css') }}" rel="stylesheet"
        type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/assets/css/forms/theme-checkbox-radio.css') }}">
@endpush
@push('start-script')
    <script src="{{ asset('assets/plugins/table/datatable/datatables.js') }}"></script>
    <script src="{{ asset('assets/assets/js/scrollspyNav.js') }}"></script>
    <script src="{{ asset('assets/plugins/file-upload/file-upload-with-preview.min.js') }}"></script>
    <script>
        //First upload
       var firstUpload = new FileUploadWithPreview('myFirstImage')
        //Second upload
        {{-- var secondUpload = new FileUploadWithPreview('mySecondImage') --}}
    </script>
    <script>
        var table = $('#default-ordering').DataTable({
            ajax: "{{ route('get_all_questions') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'question',
                    name: 'question'
                },
                // {
                //     data: 'option1',
                //     name: 'option1'
                // },
                // {
                //     data: 'option2',
                //     name: 'option2'
                // },
                {
                    data: 'answer_status',
                    name: 'answer_status'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'action',
                    name: 'action'
                },


            ],
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });
    </script>


    <script>
        function delete_question(id) {
            swal.fire({
                title: "Are you sure?",
                text: "Question will be deleted!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "GET",
                        url: '{{ url('admin/question/delete') }}/' + id,
                        success: function(data) {
                            $('#default-ordering').DataTable().ajax.reload();
                            swal.fire("Success", " Question deleted successfully", "success");
                        },
                        error: function(error) {
                            Snackbar.show({
                                text: 'Server error',
                                pos: 'top-right',
                                actionTextColor: '#fff',
                                backgroundColor: '#e7515a'
                            });
                        }
                    });
                } else {
                    Swal.fire('Question not deleted', '', 'info')
                }
            });


        }
        function change_status(id,current_status) {
            swal.fire({
                title: "Are you sure?",
                text: current_status === 1 ? "Question will not be visible to users if disabled!" : "Question will be activated and visible to users",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, change it!",
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "GET",
                        url: '{{ url('admin/qestion/status') }}/' + id,

                        success: function(data) {
                            $('#default-ordering').DataTable().ajax.reload();
                            swal.fire("Success", "status change successfully", "success");
                        },
                        error: function(error) {
                            Snackbar.show({
                                text: 'Server error',
                                pos: 'top-right',
                                actionTextColor: '#fff',
                                backgroundColor: '#e7515a'
                            });
                        }
                    });
                }
            });


        }
        function edit_question_modal(id) {
            $('#question_id').val(id);
             $.ajax({
                type: "get",
                url: '{{ url('admin/get/question') }}/' + id,
                success: function(data) {
                    $('#editquestion').val(data.question);
                    // $('#editoption1').val(data.option1);
                    // $('#editoption2').val(data.option2);
                    var option = data.answer == 1 ? 'TRUE' : 'FALSE';
                    $('#editanswer').val(option);
                    $('#edit_question_id').val(data.id);
                    $('#question_edit_modal').modal('show');
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                    Snackbar.show({
                        text: 'Failed to fetch question',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#e7515a'
                    });
                }
            });
        }

        function editquestion(){
            var question = $('#editquestion').val();
            // var option1 = $('#editoption1').val();
            // var option2 = $('#editoption2').val();
            var answer = $('#editanswer').val();
            if(answer == 'true' || answer == 'TRUE' || answer == true ){
                answer = 1;
            }
            else{
                answer = 0;
            }
            var id = $('#edit_question_id').val();
               $.ajax({
                type: "get",
                url: '{{ url('admin/update/question') }}/' + id,
                data:{
                    'question': question,
                    // 'option1': option1,
                    // 'option2': option2,
                    'answer': answer
                },
                success: function(data) {
                    $('#editquestion').val(data.question);
                    // $('#editoption1').val(data.option1);
                    // $('#editoption2').val(data.option2);
                    $('#editanswer').val(data.answer);
                    $('#question_edit_modal').modal('hide');
                    $('#default-ordering').DataTable().ajax.reload();
                    swal.fire("Success", " Question updated successfully", "success");
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                    Snackbar.show({
                        text: 'Failed to fetch question',
                        pos: 'top-right',
                        actionTextColor: '#fff',
                        backgroundColor: '#e7515a'
                    });
                }
            });
        }

        function addquestion(){
            $('#questions_modal').modal('show');
        }
        function uploadquestionsmodal(){
            //   var newInput = $("<input type='file' class='custom-file-container__custom-file__custom-file-input' id='excel_file' name='excel_file' accept='.xlsx, .xls'>");

            //   $("#excel_file").replaceWith(newInput);
              $('#upload_questions_modal').modal('show');
        }

    function uploadquestions() {
        // $("#excel_file").empty();
        var formData = new FormData($('#upload_question_form')[0]);
        $.ajax({
            type: "POST",
            url: '{{ url('upload/question') }}',
            data: formData, // Use FormData to include the file data
            contentType: false, // Set contentType to false, as FormData will set it correctly
            processData: false, // Set processData to false to prevent jQuery from processing the data
            success: function(data) {
                $('#upload_questions_modal').modal('hide');
                $('#default-ordering').DataTable().ajax.reload();
                if (data === "success") {
                    swal.fire("Success", "Question added successfully", "success");
                } else {
                    swal.fire("Error", "Some error occurred. Contact system developers if the issue persists.", "error");
                }
                location.reload();
            },
            error: function(error) {
                Snackbar.show({
                    text: 'Server error',
                    pos: 'top-right',
                    actionTextColor: '#fff',
                    backgroundColor: '#e7515a'
                });
            }
        });
    }

    function submitquestion(){
        $("#answer").prop("checked", false);
        // $("#option1").empty();
        // $("#option2").empty();
        $("#question").empty();
            $.ajax({
            type: "POST",
            url: '{{ url('admin/question/add') }}',
            data: $('#add_question_form').serialize(),
            success: function(data) {
                $('#questions_modal').modal('hide');
                $('#default-ordering').DataTable().ajax.reload();
                $('#add_question_form :input:not([type="hidden"])').val('');
                if(data === "success"){
                    swal.fire("Success", "question added successfully", "success");
                }
                else{
                    swal.fire("error", " Some error occured contact system developers if issue persist", "error");
                }

            },
            error: function(error) {
                console.log(error.responseJSON.errors);
                let errors =  error.responseJSON.errors;
                // console.log(errors.question.length);
                if(errors.answer && errors.answer.length){
                    console.log(errors.answer[0]);
                    $("#answer").text(errors.answer[0])
                }
                // if(errors.option1 && errors.option1.length){
                //     console.log(errors.option1[0]);
                //     $("#option1").text(errors.option1[0])
                // }
                // if(errors.option2 && errors.option2.length){
                //     console.log(errors.option2[0]);
                //     $("#option2").text(errors.option2[0])
                // }
                if(errors.question && errors.question.length){
                    console.log(errors.question[0]);
                    $("#question").text(errors.question[0])
                }
            }
        });

    }

    </script>
@endpush

@section('content')
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row layout-top-spacing">
                <a type="button" class="btn btn-warning ml-3 mb-3" onclick="addquestion()" href="#">Add
                    Question</a>
                    <a type="button" class="btn btn-warning ml-3 mb-3" onclick="uploadquestionsmodal()" href="#">Upload
                    Questions</a>
                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                    <div class="widget-content widget-content-area br-6">

                        <table id="default-ordering" class="table dt-table-hover" style="width:100%">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Question</th>
                                    {{-- <th>Option1</th>
                                    <th>Option2</th> --}}
                                    <th>Answer</th>
                                    <th>Status</th>
                                    {{-- <th>Paragraph</th>
                                    <th>Status</th> --}}
                                    <th class="no-content">Actions</th>
                                </tr>
                            </thead>

                        </table>
                    </div>
                </div>

            </div>

        </div>

        <!-- Button trigger modal -->


    <!-- add question modal -->
    <div class="modal fade" id="questions_modal" tabindex="-1" role="dialog" aria-labelledby="questions_modal_modalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="questions_modal_modalTitle">Add Question</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form id="add_question_form">
            @csrf

                    <div class="container mt-5">
                      <div class="form-floating mb-3">
                      <label for="floatingInput">Question</label>
                        <input type="text" class="form-control" id="questioninput" name="question_input" placeholder="Write question" required>
                        <span class="error" id="question"></span>
                        </div>
                         {{-- <div class="form-floating mb-3">
                      <label for="floatingInput">Option1</label>
                        <input type="text" class="form-control" id="questioninput" name="option1" placeholder="Write question" required>
                        <span class="error" id="option1"></span>

                        </div>
                         <div class="form-floating mb-3">
                      <label for="floatingInput">Option2</label>
                        <input type="text" class="form-control" id="questioninput" name="option2" placeholder="Write question" required>
                        <span class="error" id="option2"></span>

                        </div> --}}
                        <div class="form-floating mb-3">
                            <label for="floatingInput">Answer</label>
                              <input type="text" class="form-control" id="answerinput" name="answer_input" placeholder="Set answer TRUE or FALSE" required>
                              <span class="error" id="answer"></span>
                              </div>
            </form>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="submitquestion()">Submit</button>
            </div>
          </div>
        </div>
      </div>
        {{-- <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © 2021 <a target="_blank">QuizApp</a>, All
                    rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" class="feather feather-heart">
                        <path
                            d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                        </path>
                    </svg></p>
            </div>
        </div> --}}
    </div>



    {{-- upload_questions_modal --}}

    <div class="modal fade" id="upload_questions_modal" tabindex="-1" role="dialog" aria-labelledby="upload_questions_modal_modalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="upload_questions_modal_modalTitle">Upload Questions File</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form id="upload_question_form" method="POST" enctype="multipart/form-data">
            @csrf
                    <div class="container mt-5">
                      <div class="form-floating mb-3">
                        <label for="floatingInput">Questions File</label>
                        <div class="custom-file-container" data-upload-id="myFirstImage">
                            <label>Upload (Single File) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
                            <label class="custom-file-container__custom-file" >
                                <input type="file" class="custom-file-container__custom-file__custom-file-input" id="excel_file" name="excel_file" accept=".xlsx, .xls">
                                {{-- <input type="hidden" name="MAX_FILE_SIZE" value="10485760" /> --}}
                                <span class="custom-file-container__custom-file__custom-file-control"></span>
                            </label>
                            <div class="custom-file-container__image-preview"></div>
                        </div>
                      </div>
                    </div>
            </form>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="uploadquestions()">Submit</button>
            </div>
          </div>
        </div>
      </div>
        {{-- <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © 2021 <a target="_blank">QuizApp</a>, All
                    rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" class="feather feather-heart">
                        <path
                            d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                        </path>
                    </svg></p>
            </div>
        </div> --}}
    </div>

    {{-- question_edit_modal --}}

     <div class="modal fade" id="question_edit_modal" tabindex="-1" role="dialog" aria-labelledby="question_edit_modal_modalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="question_edit_modal_modalTitle">Edit Question</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <form id="add_question_form">
            @csrf
                    <div class="container mt-5">
                      <div class="form-floating mb-3">
                      <label for="floatingInput">Question</label>
                        <input type="text" class="form-control" id="editquestion" name="question_input">
                        </div>
                         <div class="form-floating mb-3">
                      {{-- <label for="floatingInput">Option1</label>
                        <input type="text" class="form-control" id="editoption1" name="option1">

                        </div>
                         <div class="form-floating mb-3">
                      <label for="floatingInput">Option2</label>
                        <input type="text" class="form-control" id="editoption2" name="option2">
                        </div> --}}
                        <div class="form-floating">
                        <label for="floatingPassword">Answer</label>
                        <input type="text" class="form-control" id="editanswer" name="answer_input">
                        </div>
                         <input type="hidden" class="form-control" id="edit_question_id" name="edit_question_id">
                    </div>
            </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="editquestion()">Submit</button>
            </div>
          </div>
        </div>
      </div>
        {{-- <div class="footer-wrapper">
            <div class="footer-section f-section-1">
                <p class="">Copyright © 2021 <a target="_blank">QuizApp</a>, All
                    rights reserved.</p>
            </div>
            <div class="footer-section f-section-2">
                <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" class="feather feather-heart">
                        <path
                            d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z">
                        </path>
                    </svg></p>
            </div>
        </div> --}}
    </div>
@endsection
