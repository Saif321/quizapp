<?php

use App\Http\Controllers\Admin\ExcelController;
use App\Http\Controllers\Admin\LeaderBoardController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Admin\{UserController,SettingController,QuestionController};
use Illuminate\Support\Facades\Route;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/dashboard', function () {
    $users = User::whereHasRole('user')->get();
    $users = count($users);
    return view('admin.layouts.dashboard', compact('users'));
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::get('/get_all_users_index', [UserController::class, 'get_all_users_index'])->name('get_all_users_index');
    Route::get('/get_all_users', [UserController::class, 'get_all_users'])->name('get_all_users');
    Route::get('/set_default_user_lives', [SettingController::class, 'set_default_user_lives'])->name('set_default_user_lives');
    Route::get('/settings', [SettingController::class, 'settings_index'])->name('settings');
    Route::get('/questions', [QuestionController::class, 'questions_index'])->name('questions');
    Route::get('/get_all_questions', [QuestionController::class, 'get_all_questions'])->name('get_all_questions');
    Route::post('/admin/question/add', [QuestionController::class, 'admin_question_add']);
    Route::post('/upload/question', [ExcelController::class, 'uploadExcel']);
    Route::get('admin/question/delete/{id}', [QuestionController::class, 'admin_question_delete']);
    Route::get('admin/get/question/{id}', [QuestionController::class, 'admin_get_question']);
    Route::get('admin/update/question/{id}', [QuestionController::class, 'admin_update_question']);
    Route::get('admin/qestion/status/{id}', [QuestionController::class, 'admin_question_status']);
    Route::get('admin/user/status/{id}', [UserController::class, 'admin_change_status']);
    Route::post('update_settings}', [SettingController::class, 'update_settings'])->name('update_settings');
    Route::get('leaderboard', [LeaderBoardController::class, 'leaderboard'])->name('leaderboard');
    Route::get('get_leaderboard', [LeaderBoardController::class, 'get_leaderboard'])->name('get_leaderboard');

    Route::get('/logout', function () {
        Auth::logout();
        return redirect('/');
    })->name('logout');

});

require __DIR__.'/auth.php';
